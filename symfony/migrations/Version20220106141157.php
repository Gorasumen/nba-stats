<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220106141157 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_stats ADD id_team_id INT NOT NULL');
        $this->addSql('ALTER TABLE player_stats ADD CONSTRAINT FK_E8351CECF7F171DE FOREIGN KEY (id_team_id) REFERENCES team (id)');
        $this->addSql('CREATE INDEX IDX_E8351CECF7F171DE ON player_stats (id_team_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_stats DROP FOREIGN KEY FK_E8351CECF7F171DE');
        $this->addSql('DROP INDEX IDX_E8351CECF7F171DE ON player_stats');
        $this->addSql('ALTER TABLE player_stats DROP id_team_id');
    }
}
