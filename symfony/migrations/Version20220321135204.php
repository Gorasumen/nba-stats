<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220321135204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game CHANGE id id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE player_stats CHANGE id_game_id id_game_id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE score_game CHANGE id_game_id id_game_id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE score_game ADD CONSTRAINT FK_933B0FA3A127075 FOREIGN KEY (id_game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_933B0FA3A127075 ON score_game (id_game_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game CHANGE id id INT NOT NULL');
        $this->addSql('ALTER TABLE player_stats CHANGE id_game_id id_game_id INT NOT NULL');
        $this->addSql('ALTER TABLE score_game DROP FOREIGN KEY FK_933B0FA3A127075');
        $this->addSql('DROP INDEX IDX_933B0FA3A127075 ON score_game');
        $this->addSql('ALTER TABLE score_game CHANGE id_game_id id_game_id INT NOT NULL');
    }
}
