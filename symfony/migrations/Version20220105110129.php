<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220105110129 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE avg_ttfl (id INT AUTO_INCREMENT NOT NULL, id_player_id INT NOT NULL, type VARCHAR(15) NOT NULL, score DOUBLE PRECISION NOT NULL, INDEX IDX_996FEBB019D349F8 (id_player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE defense (id INT AUTO_INCREMENT NOT NULL, id_team_id INT DEFAULT NULL, type VARCHAR(20) NOT NULL, pos VARCHAR(5) NOT NULL, score DOUBLE PRECISION NOT NULL, INDEX IDX_DBA5F575F7F171DE (id_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, h_team_id INT NOT NULL, a_team_id INT NOT NULL, id_year_id INT NOT NULL, url_code VARCHAR(50) NOT NULL, status SMALLINT NOT NULL, game_date SMALLINT NOT NULL, start_time TIME DEFAULT NULL, INDEX IDX_232B318C39B4D1A8 (h_team_id), INDEX IDX_232B318C951767F3 (a_team_id), INDEX IDX_232B318C9E5C57D7 (id_year_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, id_team_id INT DEFAULT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, display_name VARCHAR(100) NOT NULL, jersey SMALLINT NOT NULL, pos VARCHAR(5) NOT NULL, birthday DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_98197A65F7F171DE (id_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_stats (id INT AUTO_INCREMENT NOT NULL, id_player_id INT NOT NULL, id_game_id INT NOT NULL, pos VARCHAR(5) DEFAULT NULL, points SMALLINT NOT NULL, minutes SMALLINT NOT NULL, fgm SMALLINT NOT NULL, fga SMALLINT NOT NULL, fgp DOUBLE PRECISION NOT NULL, ftm SMALLINT NOT NULL, fta SMALLINT NOT NULL, ftp DOUBLE PRECISION NOT NULL, tpm SMALLINT NOT NULL, tpa SMALLINT NOT NULL, tpp DOUBLE PRECISION NOT NULL, rebounds SMALLINT NOT NULL, assists SMALLINT NOT NULL, fouls SMALLINT NOT NULL, steals SMALLINT NOT NULL, blocks SMALLINT NOT NULL, score_ttfl SMALLINT NOT NULL, INDEX IDX_E8351CEC19D349F8 (id_player_id), INDEX IDX_E8351CEC3A127075 (id_game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE score_game (id INT AUTO_INCREMENT NOT NULL, id_game_id INT NOT NULL, type VARCHAR(20) NOT NULL, score SMALLINT NOT NULL, INDEX IDX_933B0FA3A127075 (id_game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stat_ttflb2b (id INT AUTO_INCREMENT NOT NULL, id_player_id INT NOT NULL, type VARCHAR(15) NOT NULL, score DOUBLE PRECISION NOT NULL, INDEX IDX_931F921519D349F8 (id_player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stat_ttflcourt (id INT AUTO_INCREMENT NOT NULL, id_player_id INT NOT NULL, type VARCHAR(15) NOT NULL, court VARCHAR(10) NOT NULL, score DOUBLE PRECISION NOT NULL, INDEX IDX_3F6270F719D349F8 (id_player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, tricode VARCHAR(3) NOT NULL, city VARCHAR(50) NOT NULL, conference VARCHAR(10) NOT NULL, division VARCHAR(20) NOT NULL, url_name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE year (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(15) NOT NULL, year SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE avg_ttfl ADD CONSTRAINT FK_996FEBB019D349F8 FOREIGN KEY (id_player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE defense ADD CONSTRAINT FK_DBA5F575F7F171DE FOREIGN KEY (id_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C39B4D1A8 FOREIGN KEY (h_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C951767F3 FOREIGN KEY (a_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C9E5C57D7 FOREIGN KEY (id_year_id) REFERENCES year (id)');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65F7F171DE FOREIGN KEY (id_team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE player_stats ADD CONSTRAINT FK_E8351CEC19D349F8 FOREIGN KEY (id_player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_stats ADD CONSTRAINT FK_E8351CEC3A127075 FOREIGN KEY (id_game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE score_game ADD CONSTRAINT FK_933B0FA3A127075 FOREIGN KEY (id_game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE stat_ttflb2b ADD CONSTRAINT FK_931F921519D349F8 FOREIGN KEY (id_player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE stat_ttflcourt ADD CONSTRAINT FK_3F6270F719D349F8 FOREIGN KEY (id_player_id) REFERENCES player (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE player_stats DROP FOREIGN KEY FK_E8351CEC3A127075');
        $this->addSql('ALTER TABLE score_game DROP FOREIGN KEY FK_933B0FA3A127075');
        $this->addSql('ALTER TABLE avg_ttfl DROP FOREIGN KEY FK_996FEBB019D349F8');
        $this->addSql('ALTER TABLE player_stats DROP FOREIGN KEY FK_E8351CEC19D349F8');
        $this->addSql('ALTER TABLE stat_ttflb2b DROP FOREIGN KEY FK_931F921519D349F8');
        $this->addSql('ALTER TABLE stat_ttflcourt DROP FOREIGN KEY FK_3F6270F719D349F8');
        $this->addSql('ALTER TABLE defense DROP FOREIGN KEY FK_DBA5F575F7F171DE');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C39B4D1A8');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C951767F3');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A65F7F171DE');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C9E5C57D7');
        $this->addSql('DROP TABLE avg_ttfl');
        $this->addSql('DROP TABLE defense');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE player_stats');
        $this->addSql('DROP TABLE score_game');
        $this->addSql('DROP TABLE stat_ttflb2b');
        $this->addSql('DROP TABLE stat_ttflcourt');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE year');
    }
}
