<?php

namespace App\Command;

use App\Service\GetApiData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdatePlayerCommand extends Command
{
    private $getApiData;

    protected static $defaultName = 'app:data:updatePlayer';

    public function __construct(GetApiData $getApiData)
    {
        $this->getApiData = $getApiData;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Ajoute les joueurs à la base de données')
            ->addArgument('year', InputArgument::OPTIONAL, "La saison souhaitée")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $year = 2021; //année par défaut

        $yearOpt = $input->getArgument('year');
        if ($yearOpt) {
            $year = $yearOpt;
        }

        $count = $this->getApiData->getPlayer($year);

        $io->success(sprintf('Modification de "%d" joueur(s).', $count));

        return 0;
    }
}