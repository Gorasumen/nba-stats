<?php

namespace App\Command;

use App\Repository\GameRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SaveNightGame extends Command
{
    private $gameRepositiory;

    protected static $defaultName = 'app:data:saveNightGame';

    public function __construct(GameRepository $gameRepositiory)
    {
        $this->gameRepositiory = $gameRepositiory;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Lance les commandes pour enregister les données des matchs de la nuit')
            ->addArgument('gameDate', InputArgument::REQUIRED, "La date ddes matchs désirés")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $gameDate = $input->getArgument('gameDate');

        $result = $this->gameRepositiory->findBy(array(
            "gameDate" => $gameDate
        ));

        if(!empty($result))
        {
            foreach($result as $game)
            {
                exec('cd ' .dirname(__DIR__, 2).' && php bin/console app:data:saveScore ' .$game->getId());
                $io->success(sprintf('Enregistrement des match du soir : "%s".',dirname(__DIR__, 2)));
            }
           
        }
        else 
        {
            $io->error(sprintf('Erreur !'));
        }

        return 0;
    }
}