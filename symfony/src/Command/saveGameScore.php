<?php

namespace App\Command;

use App\Service\GetApiData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SaveGameScore extends Command
{
    private $getApiData;

    protected static $defaultName = 'app:data:saveScore';

    public function __construct(GetApiData $getApiData)
    {
        $this->getApiData = $getApiData;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Enregistre les scores du match dans la base de données')
            ->addArgument('gameId', InputArgument::REQUIRED, "L'id de la game")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $gameId = $input->getArgument('gameId');

        $result = $this->getApiData->saveScore($gameId);

        if($result){
            $io->success(sprintf('Enregistrement du match id : "%s".', $result));
        }
        else {
            $io->error(sprintf('Erreur !'));
        }

        return 0;
    }
}