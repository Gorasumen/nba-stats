<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 25)]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $urlCode;

    #[ORM\Column(type: 'smallint')]
    private $status;

    #[ORM\Column(type: 'integer')]
    private $gameDate;

    #[ORM\Column(type: 'time', nullable: true)]
    private $startTime;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'gamesHome')]
    #[ORM\JoinColumn(nullable: false)]
    private $hTeam;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'gamesAway')]
    #[ORM\JoinColumn(nullable: false)]
    private $aTeam;

    #[ORM\ManyToOne(targetEntity: Year::class, inversedBy: 'games')]
    #[ORM\JoinColumn(nullable: false)]
    private $idYear;

    #[ORM\OneToMany(mappedBy: 'idGame', targetEntity: ScoreGame::class, orphanRemoval: true)]
    private $scoreGames;

    #[ORM\OneToMany(mappedBy: 'idGame', targetEntity: PlayerStats::class)]
    private $playerStats;

    public function __construct()
    {
        $this->scoreGames = new ArrayCollection();
        $this->playerStats = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    public function getUrlCode(): ?string
    {
        return $this->urlCode;
    }

    public function setUrlCode(string $urlCode): self
    {
        $this->urlCode = $urlCode;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGameDate(): ?int
    {
        return $this->gameDate;
    }

    public function setGameDate(int $gameDate): self
    {
        $this->gameDate = $gameDate;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getHTeam(): ?Team
    {
        return $this->hTeam;
    }

    public function setHTeam(?Team $hTeam): self
    {
        $this->hTeam = $hTeam;

        return $this;
    }

    public function getATeam(): ?Team
    {
        return $this->aTeam;
    }

    public function setATeam(?Team $aTeam): self
    {
        $this->aTeam = $aTeam;

        return $this;
    }

    public function getIdYear(): ?Year
    {
        return $this->idYear;
    }

    public function setIdYear(?Year $idYear): self
    {
        $this->idYear = $idYear;

        return $this;
    }

    /**
     * @return Collection|ScoreGame[]
     */
    public function getScoreGames(): Collection
    {
        return $this->scoreGames;
    }

    public function addScoreGame(ScoreGame $scoreGame): self
    {
        if (!$this->scoreGames->contains($scoreGame)) {
            $this->scoreGames[] = $scoreGame;
            $scoreGame->setIdGame($this);
        }

        return $this;
    }

    public function removeScoreGame(ScoreGame $scoreGame): self
    {
        if ($this->scoreGames->removeElement($scoreGame)) {
            // set the owning side to null (unless already changed)
            if ($scoreGame->getIdGame() === $this) {
                $scoreGame->setIdGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayerStats[]
     */
    public function getPlayerStats(): Collection
    {
        return $this->playerStats;
    }

    public function addPlayerStat(PlayerStats $playerStat): self
    {
        if (!$this->playerStats->contains($playerStat)) {
            $this->playerStats[] = $playerStat;
            $playerStat->setIdGame($this);
        }

        return $this;
    }

    public function removePlayerStat(PlayerStats $playerStat): self
    {
        if ($this->playerStats->removeElement($playerStat)) {
            // set the owning side to null (unless already changed)
            if ($playerStat->getIdGame() === $this) {
                $playerStat->setIdGame(null);
            }
        }

        return $this;
    }
}
