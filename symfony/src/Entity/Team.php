<?php

namespace App\Entity;

use App\Repository\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeamRepository::class)]
class Team
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $name;

    #[ORM\Column(type: 'string', length: 3)]
    private $tricode;

    #[ORM\Column(type: 'string', length: 50)]
    private $city;

    #[ORM\Column(type: 'string', length: 10)]
    private $conference;

    #[ORM\Column(type: 'string', length: 20)]
    private $division;

    #[ORM\Column(type: 'string', length: 100)]
    private $urlName;

    #[ORM\OneToMany(mappedBy: 'idTeam', targetEntity: Player::class)]
    private $players;

    #[ORM\OneToMany(mappedBy: 'idTeam', targetEntity: Defense::class)]
    private $defenses;

    #[ORM\OneToMany(mappedBy: 'hTeam', targetEntity: Game::class)]
    private $gamesHome;

    #[ORM\OneToMany(mappedBy: 'aTeam', targetEntity: Game::class)]
    private $gamesAway;

    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->defenses = new ArrayCollection();
        $this->gamesHome = new ArrayCollection();
        $this->gamesAway = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set id
     * @param integer $id
     * @return Team
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTricode(): ?string
    {
        return $this->tricode;
    }

    public function setTricode(string $tricode): self
    {
        $this->tricode = $tricode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getConference(): ?string
    {
        return $this->conference;
    }

    public function setConference(string $conference): self
    {
        $this->conference = $conference;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getUrlName(): ?string
    {
        return $this->urlName;
    }

    public function setUrlName(string $urlName): self
    {
        $this->urlName = $urlName;

        return $this;
    }

    /**
     * @return Collection|Player[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setIdTeam($this);
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        if ($this->players->removeElement($player)) {
            // set the owning side to null (unless already changed)
            if ($player->getIdTeam() === $this) {
                $player->setIdTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Defense[]
     */
    public function getDefenses(): Collection
    {
        return $this->defenses;
    }

    public function addDefense(Defense $defense): self
    {
        if (!$this->defenses->contains($defense)) {
            $this->defenses[] = $defense;
            $defense->setIdTeam($this);
        }

        return $this;
    }

    public function removeDefense(Defense $defense): self
    {
        if ($this->defenses->removeElement($defense)) {
            // set the owning side to null (unless already changed)
            if ($defense->getIdTeam() === $this) {
                $defense->setIdTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGamesHome(): Collection
    {
        return $this->gamesHome;
    }

    public function addGamesHome(Game $gamesHome): self
    {
        if (!$this->gamesHome->contains($gamesHome)) {
            $this->gamesHome[] = $gamesHome;
            $gamesHome->setHTeam($this);
        }

        return $this;
    }

    public function removeGamesHome(Game $gamesHome): self
    {
        if ($this->gamesHome->removeElement($gamesHome)) {
            // set the owning side to null (unless already changed)
            if ($gamesHome->getHTeam() === $this) {
                $gamesHome->setHTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGamesAway(): Collection
    {
        return $this->gamesAway;
    }

    public function addGamesAway(Game $gamesAway): self
    {
        if (!$this->gamesAway->contains($gamesAway)) {
            $this->gamesAway[] = $gamesAway;
            $gamesAway->setATeam($this);
        }

        return $this;
    }

    public function removeGamesAway(Game $gamesAway): self
    {
        if ($this->gamesAway->removeElement($gamesAway)) {
            // set the owning side to null (unless already changed)
            if ($gamesAway->getATeam() === $this) {
                $gamesAway->setATeam(null);
            }
        }

        return $this;
    }
}
