<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
class Player
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $firstName;

    #[ORM\Column(type: 'string', length: 50)]
    private $lastName;

    #[ORM\Column(type: 'string', length: 100)]
    private $displayName;

    #[ORM\Column(type: 'smallint')]
    private $jersey;

    #[ORM\Column(type: 'string', length: 5)]
    private $pos;

    #[ORM\Column(type: 'date_immutable', nullable: true)]
    private $birthday;

    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'players')]
    private $idTeam;

    #[ORM\OneToMany(mappedBy: 'idPlayer', targetEntity: PlayerStats::class)]
    private $playerStats;

    #[ORM\OneToMany(mappedBy: 'idPlayer', targetEntity: StatTTFLb2b::class, orphanRemoval: true)]
    private $statTTFLb2bs;

    #[ORM\OneToMany(mappedBy: 'idPlayer', targetEntity: AvgTTFL::class, orphanRemoval: true)]
    private $avgTTFLs;

    #[ORM\OneToMany(mappedBy: 'idPlayer', targetEntity: StatTTFLcourt::class, orphanRemoval: true)]
    private $statTTFLcourts;

    public function __construct()
    {
        $this->playerStats = new ArrayCollection();
        $this->statTTFLb2bs = new ArrayCollection();
        $this->avgTTFLs = new ArrayCollection();
        $this->statTTFLcourts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getJersey(): ?int
    {
        return $this->jersey;
    }

    public function setJersey(int $jersey): self
    {
        $this->jersey = $jersey;

        return $this;
    }

    public function getPos(): ?string
    {
        return $this->pos;
    }

    public function setPos(string $pos): self
    {
        $this->pos = $pos;

        return $this;
    }

    public function getBirthday(): ?\DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeImmutable $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getIdTeam(): ?Team
    {
        return $this->idTeam;
    }

    public function setIdTeam(?Team $idTeam): self
    {
        $this->idTeam = $idTeam;

        return $this;
    }

    /**
     * @return Collection|PlayerStats[]
     */
    public function getPlayerStats(): Collection
    {
        return $this->playerStats;
    }

    public function addPlayerStat(PlayerStats $playerStat): self
    {
        if (!$this->playerStats->contains($playerStat)) {
            $this->playerStats[] = $playerStat;
            $playerStat->setIdPlayer($this);
        }

        return $this;
    }

    public function removePlayerStat(PlayerStats $playerStat): self
    {
        if ($this->playerStats->removeElement($playerStat)) {
            // set the owning side to null (unless already changed)
            if ($playerStat->getIdPlayer() === $this) {
                $playerStat->setIdPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatTTFLb2b[]
     */
    public function getStatTTFLb2bs(): Collection
    {
        return $this->statTTFLb2bs;
    }

    public function addStatTTFLb2b(StatTTFLb2b $statTTFLb2b): self
    {
        if (!$this->statTTFLb2bs->contains($statTTFLb2b)) {
            $this->statTTFLb2bs[] = $statTTFLb2b;
            $statTTFLb2b->setIdPlayer($this);
        }

        return $this;
    }

    public function removeStatTTFLb2b(StatTTFLb2b $statTTFLb2b): self
    {
        if ($this->statTTFLb2bs->removeElement($statTTFLb2b)) {
            // set the owning side to null (unless already changed)
            if ($statTTFLb2b->getIdPlayer() === $this) {
                $statTTFLb2b->setIdPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AvgTTFL[]
     */
    public function getAvgTTFLs(): Collection
    {
        return $this->avgTTFLs;
    }

    public function addAvgTTFL(AvgTTFL $avgTTFL): self
    {
        if (!$this->avgTTFLs->contains($avgTTFL)) {
            $this->avgTTFLs[] = $avgTTFL;
            $avgTTFL->setIdPlayer($this);
        }

        return $this;
    }

    public function removeAvgTTFL(AvgTTFL $avgTTFL): self
    {
        if ($this->avgTTFLs->removeElement($avgTTFL)) {
            // set the owning side to null (unless already changed)
            if ($avgTTFL->getIdPlayer() === $this) {
                $avgTTFL->setIdPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatTTFLcourt[]
     */
    public function getStatTTFLcourts(): Collection
    {
        return $this->statTTFLcourts;
    }

    public function addStatTTFLcourt(StatTTFLcourt $statTTFLcourt): self
    {
        if (!$this->statTTFLcourts->contains($statTTFLcourt)) {
            $this->statTTFLcourts[] = $statTTFLcourt;
            $statTTFLcourt->setIdPlayer($this);
        }

        return $this;
    }

    public function removeStatTTFLcourt(StatTTFLcourt $statTTFLcourt): self
    {
        if ($this->statTTFLcourts->removeElement($statTTFLcourt)) {
            // set the owning side to null (unless already changed)
            if ($statTTFLcourt->getIdPlayer() === $this) {
                $statTTFLcourt->setIdPlayer(null);
            }
        }

        return $this;
    }
}
