<?php

namespace App\Entity;

use App\Repository\PlayerStatsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlayerStatsRepository::class)]
class PlayerStats
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 5, nullable: true)]
    private $pos;

    #[ORM\Column(type: 'smallint')]
    private $points;

    #[ORM\Column(type: 'smallint')]
    private $minutes;

    #[ORM\Column(type: 'smallint')]
    private $fgm;

    #[ORM\Column(type: 'smallint')]
    private $fga;

    #[ORM\Column(type: 'float')]
    private $fgp;

    #[ORM\Column(type: 'smallint')]
    private $ftm;

    #[ORM\Column(type: 'smallint')]
    private $fta;

    #[ORM\Column(type: 'float')]
    private $ftp;

    #[ORM\Column(type: 'smallint')]
    private $tpm;

    #[ORM\Column(type: 'smallint')]
    private $tpa;

    #[ORM\Column(type: 'float')]
    private $tpp;

    #[ORM\Column(type: 'smallint')]
    private $rebounds;

    #[ORM\Column(type: 'smallint')]
    private $assists;

    #[ORM\Column(type: 'smallint')]
    private $fouls;

    #[ORM\Column(type: 'smallint')]
    private $steals;

    #[ORM\Column(type: 'smallint')]
    private $blocks;

    #[ORM\Column(type: 'smallint')]
    private $turnovers;

    #[ORM\Column(type: 'smallint')]
    private $scoreTTFL;

    #[ORM\ManyToOne(targetEntity: Player::class, inversedBy: 'playerStats')]
    #[ORM\JoinColumn(nullable: false)]
    private $idPlayer;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'playerStats')]
    #[ORM\JoinColumn(nullable: false)]
    private $idGame;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $idTeam;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPos(): ?string
    {
        return $this->pos;
    }

    public function setPos(?string $pos): self
    {
        $this->pos = $pos;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function getMinutes(): ?int
    {
        return $this->minutes;
    }

    public function setMinutes(int $minutes): self
    {
        $this->minutes = $minutes;

        return $this;
    }

    public function getFgm(): ?int
    {
        return $this->fgm;
    }

    public function setFgm(int $fgm): self
    {
        $this->fgm = $fgm;

        return $this;
    }

    public function getFga(): ?int
    {
        return $this->fga;
    }

    public function setFga(int $fga): self
    {
        $this->fga = $fga;

        return $this;
    }

    public function getFgp(): ?float
    {
        return $this->fgp;
    }

    public function setFgp(float $fgp): self
    {
        $this->fgp = $fgp;

        return $this;
    }

    public function getFtm(): ?int
    {
        return $this->ftm;
    }

    public function setFtm(int $ftm): self
    {
        $this->ftm = $ftm;

        return $this;
    }

    public function getFta(): ?int
    {
        return $this->fta;
    }

    public function setFta(int $fta): self
    {
        $this->fta = $fta;

        return $this;
    }

    public function getFtp(): ?float
    {
        return $this->ftp;
    }

    public function setFtp(float $ftp): self
    {
        $this->ftp = $ftp;

        return $this;
    }

    public function getTpm(): ?int
    {
        return $this->tpm;
    }

    public function setTpm(int $tpm): self
    {
        $this->tpm = $tpm;

        return $this;
    }

    public function getTpa(): ?int
    {
        return $this->tpa;
    }

    public function setTpa(int $tpa): self
    {
        $this->tpa = $tpa;

        return $this;
    }

    public function getTpp(): ?float
    {
        return $this->tpp;
    }

    public function setTpp(float $tpp): self
    {
        $this->tpp = $tpp;

        return $this;
    }

    public function getRebounds(): ?int
    {
        return $this->rebounds;
    }

    public function setRebounds(int $rebounds): self
    {
        $this->rebounds = $rebounds;

        return $this;
    }

    public function getAssists(): ?int
    {
        return $this->assists;
    }

    public function setAssists(int $assists): self
    {
        $this->assists = $assists;

        return $this;
    }

    public function getFouls(): ?int
    {
        return $this->fouls;
    }

    public function setFouls(int $fouls): self
    {
        $this->fouls = $fouls;

        return $this;
    }

    public function getSteals(): ?int
    {
        return $this->steals;
    }

    public function setSteals(int $steals): self
    {
        $this->steals = $steals;

        return $this;
    }

    public function getBlocks(): ?int
    {
        return $this->blocks;
    }

    public function setBlocks(int $blocks): self
    {
        $this->blocks = $blocks;

        return $this;
    }

    public function getTurnovers(): ?int
    {
        return $this->turnovers;
    }

    public function setTurnovers(int $turnovers): self
    {
        $this->turnovers = $turnovers;

        return $this;
    }

    public function getScoreTTFL(): ?int
    {
        return $this->scoreTTFL;
    }

    public function setScoreTTFL(int $scoreTTFL): self
    {
        $this->scoreTTFL = $scoreTTFL;

        return $this;
    }

    public function getIdPlayer(): ?Player
    {
        return $this->idPlayer;
    }

    public function setIdPlayer(?Player $idPlayer): self
    {
        $this->idPlayer = $idPlayer;

        return $this;
    }

    public function getIdGame(): ?Game
    {
        return $this->idGame;
    }

    public function setIdGame(?Game $idGame): self
    {
        $this->idGame = $idGame;

        return $this;
    }

    public function getIdTeam(): ?Team
    {
        return $this->idTeam;
    }

    public function setIdTeam(?Team $idTeam): self
    {
        $this->idTeam = $idTeam;

        return $this;
    }
}
