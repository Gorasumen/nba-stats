<?php

namespace App\Repository;

use App\Entity\StatTTFLcourt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatTTFLcourt|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatTTFLcourt|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatTTFLcourt[]    findAll()
 * @method StatTTFLcourt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatTTFLcourtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatTTFLcourt::class);
    }

    // /**
    //  * @return StatTTFLcourt[] Returns an array of StatTTFLcourt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatTTFLcourt
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
