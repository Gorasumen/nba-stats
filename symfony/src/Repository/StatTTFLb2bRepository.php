<?php

namespace App\Repository;

use App\Entity\StatTTFLb2b;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatTTFLb2b|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatTTFLb2b|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatTTFLb2b[]    findAll()
 * @method StatTTFLb2b[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatTTFLb2bRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatTTFLb2b::class);
    }

    // /**
    //  * @return StatTTFLb2b[] Returns an array of StatTTFLb2b objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatTTFLb2b
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
