<?php

namespace App\Repository;

use App\Entity\AvgTTFL;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AvgTTFL|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvgTTFL|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvgTTFL[]    findAll()
 * @method AvgTTFL[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvgTTFLRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvgTTFL::class);
    }

    // /**
    //  * @return AvgTTFL[] Returns an array of AvgTTFL objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AvgTTFL
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
