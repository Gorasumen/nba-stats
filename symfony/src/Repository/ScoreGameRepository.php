<?php

namespace App\Repository;

use App\Entity\ScoreGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScoreGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScoreGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScoreGame[]    findAll()
 * @method ScoreGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScoreGame::class);
    }

    // /**
    //  * @return ScoreGame[] Returns an array of ScoreGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScoreGame
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
