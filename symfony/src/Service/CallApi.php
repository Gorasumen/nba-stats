<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApi
{
    private $client;
    private $urlApi = "http://data.nba.net/10s/prod/v1/";
    private $routeBoxScore = "_boxscore.json";

    public function __construct(HttpClientInterface $client){
        $this->client = $client;
    }

    public function getJsonByYear($year, $endpoint): object
    {
        $response = $this->client->request(
            'GET',
            $this->urlApi .$year. '/'.$endpoint
        );
        return $this->responseJson($response);
       
    }

    public function getScoreNight($date, $gameId): object
    {
        $response = $this->client->request(
            'GET',
            $this->urlApi .$date. '/' .$gameId.$this->routeBoxScore
        );
        return $this->responseJson($response);
       
    }

    private function responseJson($response){
        $statusCode = $response->getStatusCode();
        
        if($statusCode !== 200)
        {
            return false;
        }

        $json = $response->getContent();
        $object = json_decode($json);
      
        return $object;
    }
}