<?php

namespace App\Service;

use DateTime;
use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Year;
use App\Entity\Player;
use DateTimeImmutable;
use App\Service\CallApi;
use App\Entity\ScoreGame;
use App\Entity\PlayerStats;
use App\Repository\GameRepository;
use App\Repository\TeamRepository;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;

class GetApiData
{
    private $callApi;
    private $em;
    private $gameRepository;
    private $playerRepository;
    private $teamRepository;
    private $routeTeam = "teams.json";
    private $routePlayer = "players.json";
    private $routeSchedule = "schedule.json";
    

    public function __construct(CallApi $callApi, EntityManagerInterface $entityManager, GameRepository $gameRepository, PlayerRepository $playerRepository, TeamRepository $teamRepository)
    {
        $this->callApi = $callApi;
        $this->em = $entityManager;
        $this->gameRepository = $gameRepository;
        $this->playerRepository = $playerRepository;
        $this->teamRepository = $teamRepository;
    }

    public function getTeam($year): int
    {     
        $content = $this->callApi->getJsonByYear($year, $this->routeTeam);
        $count = 0;

        foreach($content->league->standard as $teamData){

            $team = $this->em->getRepository(Team::class)->findOneBy(array(
                "id" => $teamData->teamId
            ));

            if(!$team){
                $team = new Team();
            }
            $count++;            

            $team->setId(intval($teamData->teamId));

            $team->setName($teamData->fullName);
            $team->setTricode($teamData->tricode);
            $team->setCity($teamData->city);
            $team->setConference($teamData->confName);
            $team->setDivision($teamData->divName);
            $team->setUrlName($teamData->urlName);

            $this->em->persist($team);

        }

        $this->em->flush();
        
        return $count;
    }

    public function getPlayer($year): int
    {     
        $content = $this->callApi->getJsonByYear($year, $this->routePlayer);
        $count = 0;

        foreach($content->league->standard as $playerData){

            $player = $this->em->getRepository(Player::class)->findOneBy(array(
                "id" => $playerData->personId
            ));

            if(!$player){
                $player = new Player();
            }
            $count++;            

            $player->setId(intval($playerData->personId));

            $player->setFirstName($playerData->firstName);
            $player->setLastName($playerData->lastName);

            //if no display name
            $displayName = isset($playerData->temporaryDisplayName) ? $playerData->temporaryDisplayName : $playerData->lastName. " " .$playerData->firstName;
            $player->setDisplayName($displayName);
            $player->setJersey(intval($playerData->jersey));
            $player->setPos($playerData->pos);

            if(!empty($playerData->dateOfBirthUTC)){
                $player->setBirthday(new DateTimeImmutable($playerData->dateOfBirthUTC));
            }
            else {
                $player->setBirthday(new DateTimeImmutable($playerData->dateOfBirthUTC));
            }
            
            //his team
            $team = $this->em->getRepository(Team::class)->findOneBy(array(
                "id" => $playerData->teamId
            ));
            $player->setIdTeam($team);

            $this->em->persist($player);

        }

        $this->em->flush();
        
        return $count;
    }

    public function getSchedule($year): int
    {     
        $content = $this->callApi->getJsonByYear($year, $this->routeSchedule);
        $count = 0;

        foreach($content->league->standard as $gameData){

            if($gameData->seasonStageId === 2 && !isset($gameData->tags))
            {
                $game = $this->gameRepository->findOneById($gameData->gameId);

                if(!$game){
                    $game = new Game();
                }
                $count++;            

                $game->setId($gameData->gameId);

                $game->setUrlCode($gameData->gameUrlCode);
                $game->setStatus($gameData->statusNum);

                
                $game->setGameDate(intval($gameData->startDateEastern));

                //change data into time for sql
                $timestamp = strtotime($gameData->startTimeUTC);
                $game->setStartTime(new DateTime($gameData->startTimeUTC));
                
                $teamH = $this->em->getRepository(Team::class)->findOneBy(array(
                    "id" => intval($gameData->hTeam->teamId)
                ));

                $game->setHTeam($teamH);

                $teamA = $this->em->getRepository(Team::class)->findOneBy(array(
                    "id" => intval($gameData->vTeam->teamId)
                ));

                $game->setATeam($teamA);
                
                //season of game
                $season = $this->em->getRepository(Year::class)->findOneBy(array(
                    "year" => $year
                ));
                $game->setIdYear($season);

                $this->em->persist($game);
            }  
        }

        $this->em->flush();
        
        return $count;
    }

    public function saveScore($gameId): int
    {     
        $game = $this->gameRepository->findOneById($gameId);

        if(!$game){
            return false;
        }

        $content = $this->callApi->getScoreNight($game->getGameDate(), $gameId);

        $homeScore = $content->basicGameData->hTeam;
        $visitorScore = $content->basicGameData->vTeam;
        
        $scoreGame = new ScoreGame();
        $scoreGame->setType("h0");
        $scoreGame->setScore($homeScore->score);
        $scoreGame->setIdGame($game);
        $this->em->persist($scoreGame);

        foreach($homeScore->linescore as $key => $scoreHData){

            $scoreGame = new ScoreGame();

            //score = h0, 4QT = h1, h2, h3, h4, OT = h5+
            $scoreGame->setType("h".($key + 1));
            $scoreGame->setScore($scoreHData->score);
            $scoreGame->setIdGame($game);
            $this->em->persist($scoreGame);
              
        }

        $scoreGame = new ScoreGame();

        $scoreGame->setType("v0");
        $scoreGame->setScore($visitorScore->score);
        $scoreGame->setIdGame($game);
        $this->em->persist($scoreGame);

        foreach($visitorScore->linescore as $key => $scoreHData){

            $scoreGame = new ScoreGame();

            //score = v0, 4QT = v1, v2, v3, v4, OT = v5+
            $scoreGame->setType("v".($key + 1));
            $scoreGame->setScore($scoreHData->score);
            $scoreGame->setIdGame($game);
            $this->em->persist($scoreGame);
              
        }

        $statsPlayer = $content->stats->activePlayers;

        foreach($statsPlayer as $stat)
        {
            $player = $this->playerRepository->findOneById(intval($stat->personId));
            $team = $this->teamRepository->findOneById(intval($stat->teamId));
        
            if($player && $team){
                $playerStats = new PlayerStats();
                $playerStats->setIdPlayer($player);
                $playerStats->setIdGame($game);
                $playerStats->setIdTeam($team);
                $playerStats->setPos($stat->pos);
                $playerStats->setPoints(intval($stat->points));
                $playerStats->setMinutes($this->minutesIntoSeconds($stat->min));
                $playerStats->setFgm(intval($stat->fgm));
                $playerStats->setFga(intval($stat->fga));
                $playerStats->setFgp(intval($stat->fgp));
                $playerStats->setFtm(intval($stat->ftm));
                $playerStats->setFta(intval($stat->fta));
                $playerStats->setFtp(intval($stat->ftp));
                $playerStats->setTpm(intval($stat->tpm));
                $playerStats->setTpa(intval($stat->tpa));
                $playerStats->setTpp(intval($stat->tpp));
                $playerStats->setRebounds(intval($stat->totReb));
                $playerStats->setAssists(intval($stat->assists));
                $playerStats->setFouls(intval($stat->pFouls));
                $playerStats->setSteals(intval($stat->steals));
                $playerStats->setBlocks(intval($stat->blocks));
                $playerStats->setTurnovers(intval($stat->turnovers));
                $playerStats->setScoreTTFL($this->calcTTFL($playerStats));
                $this->em->persist($playerStats);
            }
        }

        $this->em->flush();
        
        return $gameId;
        
    }

    private function minutesIntoSeconds($minutesStr)
    {
        //time is min:sec ex: 23:45
       $time = explode(':', $minutesStr);
       
       //we want only secondes like 23 * 60 + 45
       return $time[0] * 60 + $time[1];
    }

    private function calcTTFL($playerStats)
    {
       //calc bonus
       $ttfl = $playerStats->getPoints();
       $ttfl += $playerStats->getFgm();
       $ttfl += $playerStats->getFtm();
       $ttfl += $playerStats->getTpm();
       $ttfl += $playerStats->getRebounds();
       $ttfl += $playerStats->getAssists();
       $ttfl += $playerStats->getSteals();
       $ttfl += $playerStats->getBlocks();

       //calc malus
       $ttfl -= $playerStats->getTurnovers();
       $ttfl -= $playerStats->getFga() - $playerStats->getFgm();
       $ttfl -= $playerStats->getFta() - $playerStats->getFtm();
       $ttfl -= $playerStats->getTpa() - $playerStats->getTpm();
       
       return $ttfl;
    }
}